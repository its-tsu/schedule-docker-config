#!/bin/sh

cd "$(dirname "$0")" || exit
docker-compose pull
docker-compose up -d
